<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151119023333 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE VideoCategory_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE VideoTag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE app_users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE VideoComment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE Video_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE VideoVote_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE VideoCategory (id INT NOT NULL, name VARCHAR(160) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE VideoTag (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE app_users (id INT NOT NULL, salt VARCHAR(32) NOT NULL, username VARCHAR(32) NOT NULL, password VARCHAR(64) NOT NULL, email VARCHAR(64) NOT NULL, gender VARCHAR(6) NOT NULL, roles TEXT NOT NULL, first_name VARCHAR(32) NOT NULL, last_name VARCHAR(32) NOT NULL, birthday DATE NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C2502824F85E0677 ON app_users (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C2502824E7927C74 ON app_users (email)');
        $this->addSql('COMMENT ON COLUMN app_users.roles IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE VideoComment (id INT NOT NULL, video_id INT DEFAULT NULL, author_id INT DEFAULT NULL, message VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_829CF74629C1004E ON VideoComment (video_id)');
        $this->addSql('CREATE INDEX IDX_829CF746F675F31B ON VideoComment (author_id)');
        $this->addSql('CREATE TABLE Video (id INT NOT NULL, author_id INT DEFAULT NULL, category_id INT DEFAULT NULL, title VARCHAR(160) NOT NULL, description TEXT NOT NULL, original VARCHAR(255) NOT NULL, isCommentable BOOLEAN NOT NULL, accessLevel VARCHAR(255) NOT NULL, rating BIGINT NOT NULL, likes BIGINT NOT NULL, disLikes BIGINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BD06F528F675F31B ON Video (author_id)');
        $this->addSql('CREATE INDEX IDX_BD06F52812469DE2 ON Video (category_id)');
        $this->addSql('CREATE TABLE videos_tags (video_id INT NOT NULL, tag_id INT NOT NULL, PRIMARY KEY(video_id, tag_id))');
        $this->addSql('CREATE INDEX IDX_CD9528D229C1004E ON videos_tags (video_id)');
        $this->addSql('CREATE INDEX IDX_CD9528D2BAD26311 ON videos_tags (tag_id)');
        $this->addSql('CREATE TABLE VideoVote (id INT NOT NULL, author_id INT DEFAULT NULL, video_id INT DEFAULT NULL, vote SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CF217407F675F31B ON VideoVote (author_id)');
        $this->addSql('CREATE INDEX IDX_CF21740729C1004E ON VideoVote (video_id)');
        $this->addSql('ALTER TABLE VideoComment ADD CONSTRAINT FK_829CF74629C1004E FOREIGN KEY (video_id) REFERENCES Video (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE VideoComment ADD CONSTRAINT FK_829CF746F675F31B FOREIGN KEY (author_id) REFERENCES app_users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Video ADD CONSTRAINT FK_BD06F528F675F31B FOREIGN KEY (author_id) REFERENCES app_users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Video ADD CONSTRAINT FK_BD06F52812469DE2 FOREIGN KEY (category_id) REFERENCES VideoCategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE videos_tags ADD CONSTRAINT FK_CD9528D229C1004E FOREIGN KEY (video_id) REFERENCES Video (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE videos_tags ADD CONSTRAINT FK_CD9528D2BAD26311 FOREIGN KEY (tag_id) REFERENCES VideoTag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE VideoVote ADD CONSTRAINT FK_CF217407F675F31B FOREIGN KEY (author_id) REFERENCES app_users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE VideoVote ADD CONSTRAINT FK_CF21740729C1004E FOREIGN KEY (video_id) REFERENCES Video (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE Video DROP CONSTRAINT FK_BD06F52812469DE2');
        $this->addSql('ALTER TABLE videos_tags DROP CONSTRAINT FK_CD9528D2BAD26311');
        $this->addSql('ALTER TABLE VideoComment DROP CONSTRAINT FK_829CF746F675F31B');
        $this->addSql('ALTER TABLE Video DROP CONSTRAINT FK_BD06F528F675F31B');
        $this->addSql('ALTER TABLE VideoVote DROP CONSTRAINT FK_CF217407F675F31B');
        $this->addSql('ALTER TABLE VideoComment DROP CONSTRAINT FK_829CF74629C1004E');
        $this->addSql('ALTER TABLE videos_tags DROP CONSTRAINT FK_CD9528D229C1004E');
        $this->addSql('ALTER TABLE VideoVote DROP CONSTRAINT FK_CF21740729C1004E');
        $this->addSql('DROP SEQUENCE VideoCategory_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE VideoTag_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE app_users_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE VideoComment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE Video_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE VideoVote_id_seq CASCADE');
        $this->addSql('DROP TABLE VideoCategory');
        $this->addSql('DROP TABLE VideoTag');
        $this->addSql('DROP TABLE app_users');
        $this->addSql('DROP TABLE VideoComment');
        $this->addSql('DROP TABLE Video');
        $this->addSql('DROP TABLE videos_tags');
        $this->addSql('DROP TABLE VideoVote');
    }
}
