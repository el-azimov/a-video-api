<?php

namespace App\Domain\Repository;

use App\Domain\Entity\User;
use App\Domain\Entity\Video;
use App\Domain\Entity\VideoVote;
use App\Domain\Exception\NotFoundException;


/**
 * Interface IVideoVoteRepository
 *
 * @author <thecold147@gmail.com>
 */
interface IVideoVoteRepository extends IRepository
{
    /**
     * @param User $author
     *
     * @return VideoVote|null
     */
    public function findByAuthorAndVideo(User $author, Video $video);

    /**
     * @param User $author
     *
     * @throws NotFoundException
     * @return VideoVote
     */
    public function findByAuthorAndVideoOrFail(User $author, Video $video);
}
