<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Video;
use App\Domain\Exception\NotFoundException;


/**
 * Interface IVideoRepository
 *
 * @author <thecold147@gmail.com>
 */
interface IVideoRepository extends IRepository
{
    /**
     * @param $id
     *
     * @return Video|null
     */
    public function find($id);

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return Video
     */
    public function findOrFail($id);
}
