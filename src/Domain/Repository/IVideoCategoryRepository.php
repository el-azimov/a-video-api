<?php

namespace App\Domain\Repository;

use App\Domain\Entity\VideoCategory;
use App\Domain\Exception\NotFoundException;

/**
 * Interface IVideoCategoryRepository
 *
 * @author <thecold147@gmail.com>
 */
interface IVideoCategoryRepository extends IRepository
{
    /**
     * @param integer $id
     *
     * @return VideoCategory|null
     */
    public function find($id);

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return VideoCategory
     */
    public function findOrFail($id);

    /**
     * @param string $name
     *
     * @return VideoCategory|null
     */
    public function findByName($name);

    /**
     * @param $name
     *
     * @throws NotFoundException
     * @return VideoCategory
     */
    public function findByNameOrFail($name);

    /**
     * @param integer $page
     * @param integer $pageSize
     *
     * @return \IteratorAggregate|\Countable
     */
    public function findAll($page, $pageSize);
}
