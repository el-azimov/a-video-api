<?php

namespace App\Domain\Repository;

use App\Domain\Entity\VideoTag;
use App\Domain\Exception\NotFoundException;


/**
 * Interface IVideoTagRepository
 *
 * @author <thecold147@gmail.com>
 */
interface IVideoTagRepository extends IRepository
{
    /**
     * @param int $id
     *
     * @return VideoTag|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @throws NotFoundException
     * @return VideoTag
     */
    public function findOrFail($id);

    /**
     * @param int $page
     * @param int $pageSize
     *
     * @return \IteratorAggregate|\Countable
     */
    public function findAll($page, $pageSize);

    /**
     * @param string $name
     *
     * @return VideoTag|null
     */
    public function findByName($name);

    /**
     * @param $name
     *
     * @throws NotFoundException
     * @return VideoTag
     */
    public function findByNameOrFail($name);
}
