<?php

namespace App\Domain\Repository;

use App\Domain\Entity\VideoComment;
use App\Domain\Exception\NotFoundException;


/**
 * Interface IVideoCommentRepository
 *
 * @author <thecold147@gmail.com>
 */
interface IVideoCommentRepository extends IRepository
{
    /**
     * @param $id
     *
     * @return VideoComment|null
     */
    public function find($id);

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return VideoComment
     */
    public function findOrFail($id);
}
