<?php

namespace App\Domain\Repository;

/**
 * Interface IRepository
 *
 * @author <thecold147@gmail.com>
 */
interface IRepository
{
    /**
     * @param object $entity
     *
     * @return void
     */
    public function add($entity);

    /**
     * @param object $entity
     *
     * @return void
     */
    public function remove($entity);

    /**
     * @param object $entity
     *
     * @return void
     */
    public function refresh($entity);
}
