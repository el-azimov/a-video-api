<?php

namespace App\Domain\Repository;

use App\Domain\Entity\User;
use App\Domain\Exception\NotFoundException;

/**
 * Interface IUserRepository
 *
 * @author <thecold147@gmail.com>
 */
interface IUserRepository extends IRepository
{

    /**
     * @param string $id
     *
     * @return User|null
     */
    public function find($id);

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return User
     */
    public function findOrFail($id);

    /**
     * @param string $username
     *
     * @return User|null
     */
    public function findByUsername($username);

    /**
     * @param $username
     *
     * @throws NotFoundException
     * @return User
     */
    public function findByUsernameOrFail($username);

    /**
     * @param string $email
     *
     * @return User|null
     */
    public function findByEmail($email);

    /**
     * @param $email
     *
     * @throws NotFoundException
     * @return User
     */
    public function findByEmailOrFail($email);

    /**
     * @param string $usernameOrEmail
     *
     * @return User|null
     */
    public function findByUsernameOrEmail($usernameOrEmail);

    /**
     * @param $usernameOrEmail
     *
     * @throws NotFoundException
     * @return User
     */
    public function findByUsernameOrEmailOrFail($usernameOrEmail);

    /**
     * @return User[]
     */
    public function findAll();

}
