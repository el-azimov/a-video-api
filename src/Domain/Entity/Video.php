<?php

namespace App\Domain\Entity;

use App\Domain\Exception\AlreadyVotedException;
use Atom\Uploader\Model\Embeddable\FileReference;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class Video
 *
 * @author <thecold147@gmail.com>
 */
class Video
{
    const ACCESS_PUBLIC = 'public';
    const ACCESS_LINK = 'link';
    const ACCESS_PRIVATE = 'private';

    private $id;

    private $title;

    private $description;

    private $author;

    private $original;

    private $createdAt;

    private $isCommentable;

    private $comments;

    private $accessLevel;

    private $tags;

    /** @var  VideoCategory */
    private $category;

    private $rating;

    private $likes;

    private $disLikes;

    public function __construct(
        FileReference $original,
        User $author,
        $title,
        $description,
        $isCommentable = true,
        $accessLevel,
        VideoCategory $category,
        array $tags
    ) {
        $this->original = $original;
        $this->author = $author;
        $this->createdAt = new \DateTime();
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->rating = 0;
        $this->likes = 0;
        $this->disLikes = 0;
        $this->update($title, $description, $isCommentable, $accessLevel, $category, $tags);
    }

    public function vote($vote, $oldVote = VideoVote::VOTE_ABSTAIN)
    {
        if ($oldVote === $vote) {
            throw new AlreadyVotedException('You are already voted to this video.');
        }
        if (VideoVote::VOTE_LIKE === $oldVote) {
            $this->likes--;
        } elseif (VideoVote::VOTE_DISLIKE === $oldVote) {
            $this->disLikes--;
        }
        if (VideoVote::VOTE_LIKE === $vote) {
            $this->likes++;
        } elseif (VideoVote::VOTE_DISLIKE === $vote) {
            $this->disLikes++;
        }
        $this->rating -= $oldVote;
        $this->rating += $vote;
    }

    public function addTag(VideoTag $tag)
    {
        $this->tags[] = $tag;
    }

    public function removeTag(VideoTag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * @param VideoTag[] $tags
     */
    private function rewriteTags(array $tags)
    {
        $tagsIds = array_map(function (VideoTag $videoTag) {
            return $videoTag->getId();
        }, $tags);
        /** @var VideoTag $tag */
        foreach ($this->tags->toArray() as $tag) {
            if (!in_array($tag->getId(), $tagsIds)) {
                $this->removeTag($tag);
            }
        }
        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) {
                $this->addTag($tag);
            }
        }
    }

    public function addComment(VideoComment $comment)
    {
        $this->comments[] = $comment;
    }

    public function removeComment(VideoComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'original' => $this->original->toArray(),
            'createdAt' => $this->createdAt,
            'isCommentable' => $this->isCommentable,
            'category' => $this->category->toArray(),
            'accessLevel' => $this->accessLevel,
            'tags' => array_map(function (VideoTag $tag) {
                return $tag->toArray();
            }, $this->tags->toArray()),
            'likes' => $this->likes,
            'disLikes' => $this->disLikes,
            'rating' => $this->rating
        ];
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function toArrayWithAuthor()
    {
        return array_merge($this->toArray(), ['author' => $this->author->toArray()]);
    }

    public function update(
        $title,
        $description,
        $isCommentable,
        $accessLevel,
        VideoCategory $category = null,
        array $tags
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->isCommentable = (bool)$isCommentable;
        $this->accessLevel = $accessLevel;
        if ($category) {
            $this->category = $category;
        }
        $this->rewriteTags($tags);
    }
}
