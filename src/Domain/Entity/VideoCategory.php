<?php

namespace App\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class VideoCategory
 *
 * @author <thecold147@gmail.com>
 */
class VideoCategory
{
    private $id;

    private $name;

    private $videos;

    public function __construct($name)
    {
        $this->videos = new ArrayCollection();
        $this->update($name);
    }

    public function update($name)
    {
        $this->name = $name;
    }

    public function addVideo(Video $video)
    {
        $this->videos[] = $video;
    }

    public function removeVideo(Video $video)
    {
        $this->videos->removeElement($video);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->name;
    }
}
