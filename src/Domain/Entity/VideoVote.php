<?php

namespace App\Domain\Entity;

/**
 * Class VideoVote
 *
 * @author <thecold147@gmail.com>
 */
class VideoVote
{
    const VOTE_ABSTAIN = 0;
    const VOTE_LIKE = 1;
    const VOTE_DISLIKE = -1;

    private $id;

    private $video;

    private $author;

    private $vote;

    public function __construct(Video $video, User $author, $vote)
    {
        $this->video = $video;
        $this->author = $author;
        $this->vote($vote);
    }

    public function vote($vote)
    {
        $oldVote = $this->vote;
        $this->vote = $vote;
        $this->video->vote($vote, $oldVote);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'vote' => $this->vote,
        ];
    }
}
