<?php

namespace App\Domain\Entity;

/**
 * Class VideoSubscribe
 *
 * @author <thecold147@gmail.com>
 */
class VideoSubscribe
{
    private $id;

    private $user;

    private $video;

    public function __construct(User $user, Video $video)
    {
        $this->user = $user;
        $this->video = $video;
    }
}
