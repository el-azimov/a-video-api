<?php

namespace App\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class VideoTag
 *
 * @author <thecold147@gmail.como>
 */
class VideoTag
{
    private $id;

    private $name;

    private $videos;

    public function __construct($name)
    {
        $this->update($name);
        $this->videos = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function update($name)
    {
        $this->name = $name;
    }

    public function addVideo(Video $video)
    {
        $this->videos[] = $video;
    }

    public function removeVideo(Video $video)
    {
        $this->videos->removeElement($video);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }

    public function __toString()
    {
        return $this->name;
    }
}
