<?php

namespace App\Domain\Entity;

/**
 * Class VideoComment
 *
 * @author <thecold147@gmail.com>
 */
class VideoComment
{
    private $id;

    private $message;

    private $author;

    private $createdAt;

    private $video;

    public function __construct($message, User $author, Video $video)
    {
        $this->video = $video;
        $this->author = $author;
        $this->update($message);
    }

    public function update($message)
    {
        $this->message = $message;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'createdAt' => $this->createdAt,
        ];
    }

    public function toArrayWithAuthor()
    {
        return array_merge($this->toArray(), ['author' => $this->author->toArray()]);
    }

    public function getAuthor()
    {
        return $this->author;
    }
}

