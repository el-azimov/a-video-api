<?php

namespace App\Domain\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class User
 *
 * @author <thecold147@gmail.com>
 */
class User implements UserInterface
{
    const STATUS_ACTIVE = 'active';
    const STATUS_UNAPPROVED = 'unapproved';

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    const ROLE_ADMIN = 'ROLE_ADMIN';

    private $id;

    private $username;

    private $password;

    private $email;

    private $gender;

    private $roles;

    private $firstName;

    private $lastName;

    private $birthday;

    private $createdAt;

    private $status;

    private $salt;

    private $videos;

    public function __construct($username, \DateTime $birthday, $password, $email, $gender, $firstName, $lastName)
    {
        $this->salt = uniqid(null, true);
        $this->createdAt = new \DateTime();
        $this->status = self::STATUS_UNAPPROVED;
        $this->roles = [];
        $this->videos = new ArrayCollection();
        $this->update($username, $birthday, $password, $email, $gender, $firstName, $lastName);
    }

    public function addVideo(Video $video)
    {
        $this->videos[] = $video;
    }

    public function removeVideo(Video $video)
    {
        $this->videos->removeElement($video);
    }

    public function hasRole($role)
    {
        return in_array($role, $this->roles);
    }

    public function getId()
    {
        return $this->id;
    }

    public function isPasswordValid($password)
    {
        return password_verify($password, $this->password);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'gender' => $this->gender,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'birthday' => $this->birthday,
            'createdAt' => $this->createdAt,
            'status' => $this->status,
            'roles' => $this->roles,
        ];
    }

    public function activate()
    {
        $this->status = self::STATUS_ACTIVE;
    }


    public function update($username, \DateTime $birthday, $password, $email, $gender, $firstName, $lastName)
    {
        $this->username = $username;
        $this->email = $email;
        $this->gender = $gender;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthday = $birthday;
        $this->status = self::STATUS_UNAPPROVED;
        $this->changePassword($password);
    }

    private function changePassword($password)
    {
        if (!$this->isPasswordValid($password) && $this->password !== $password) {
            $this->password = password_hash($password, PASSWORD_BCRYPT, ['salt' => $this->salt, 'cost' => 13]);
        }
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function eraseCredentials()
    {

    }
}
