<?php

namespace App\Domain\Exception;

use Exception;


/**
 * Class NotFoundException
 *
 * @author <thecold147@gmail.com>
 */
class NotFoundException extends \RuntimeException
{
    public function __construct($message = 'Entity not found.', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
