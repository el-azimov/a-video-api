<?php

namespace App\Domain\Exception;

/**
 * Class AlreadyVotedException
 *
 * @author <thecold147@gmail.com>
 */
class AlreadyVotedException extends \RuntimeException
{

}
