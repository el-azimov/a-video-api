<?php

namespace App\Application\Port;


/**
 * Interface IHandleResolver
 *
 * @author <thecold147@gmail.com>
 */
interface IHandleResolver
{
    /**
     * @param $command
     *
     * @return callable
     */
    public function resolve($command);

    /**
     * @param $command
     *
     * @return bool
     */
    public function isSupportsCommand($command);
}
