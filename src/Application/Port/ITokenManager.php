<?php

namespace App\Application\Port;

use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Interface ITokenManager
 *
 * @author <thecold147@gmail.com>
 */
interface ITokenManager
{
    /**
     * @param UserInterface $user
     *
     * @return string
     */
    public function create(UserInterface $user);
}
