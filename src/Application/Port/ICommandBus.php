<?php

namespace App\Application\Port;


/**
 * Interface ICommandBus
 *
 * @author <thecold147@gmail.com>
 */
interface ICommandBus
{
    /**
     * @param $command
     *
     * @return mixed
     */
    public function execute($command);
}
