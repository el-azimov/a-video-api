<?php

namespace App\Application\Port;


/**
 * Interface IAuthorizeChecker
 *
 * @author <thecold147@gmail.com>
 */
interface IAuthorizeChecker
{
    /**
     * Checks permissions to execute a command
     *
     * @param $command
     *
     * @return bool
     */
    public function isGranted($command);
}
