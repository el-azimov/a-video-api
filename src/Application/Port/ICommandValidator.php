<?php

namespace App\Application\Port;

use App\Application\Exception\BadRequestException;


/**
 * Interface ICommandValidator
 *
 * @author <thecold147@gmail.com>
 */
interface ICommandValidator
{
    /**
     * @param $command
     *
     * @throws BadRequestException
     */
    public function validate($command);
}
