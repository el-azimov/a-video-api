<?php

namespace App\Application\Handler;

use App\Application\Command\VideoTag\CreateVideoTagCommand;
use App\Application\Command\VideoTag\DeleteVideoTagByIdCommand;
use App\Application\Command\VideoTag\GetVideoTagByIdCommand;
use App\Application\Command\VideoTag\GetVideoTagsCommand;
use App\Application\Command\VideoTag\UpdateVideoTagByIdCommand;
use App\Domain\Entity\VideoTag;
use App\Domain\Repository\IVideoTagRepository;


/**
 * Class VideoTagHandler
 *
 * @author <thecold147@gmail.com>
 */
class VideoTagHandler
{
    private $tagRepo;

    public function __construct(IVideoTagRepository $tagRepo)
    {
        $this->tagRepo = $tagRepo;
    }

    public function createTag(CreateVideoTagCommand $command)
    {
        $tag = new VideoTag($command->name);
        $this->tagRepo->add($tag);
        return $tag->toArray();
    }

    public function updateTagById(UpdateVideoTagByIdCommand $command)
    {
        $tag = $this->tagRepo->findOrFail($command->id);
        $tag->update($command->name);
    }

    public function getTagById(GetVideoTagByIdCommand $command)
    {
        $tag = $this->tagRepo->findOrFail($command->id);
        return $tag->toArray();
    }

    public function deleteTagById(DeleteVideoTagByIdCommand $command)
    {
        $tag = $this->tagRepo->findOrFail($command->id);
        $this->tagRepo->remove($tag);
    }

    public function getTags(GetVideoTagsCommand $command)
    {
        $pageSize = $command->pageSize ?: 20;
        $tags = $this->tagRepo->findAll($command->page, $pageSize);
        $items = array_map([$this, 'tagToArray'], iterator_to_array($tags));
        $pagesCount = ceil($tags->count() / $pageSize);

        return compact('items', 'pagesCount');
    }

    private function tagToArray(VideoTag $tag)
    {
        return $tag->toArray();
    }
}
