<?php

namespace App\Application\Handler;

use App\Application\Command\Video\CreateVideoCommand;
use App\Application\Command\Video\DeleteVideoCommand;
use App\Application\Command\Video\GetVideoByIdCommand;
use App\Application\Command\Video\UpdateVideoCommand;
use App\Application\Exception\AccessDeniedException;
use App\Domain\Entity\Video;
use App\Domain\Entity\VideoTag;
use App\Domain\Repository\IUserRepository;
use App\Domain\Repository\IVideoCategoryRepository;
use App\Domain\Repository\IVideoRepository;
use App\Domain\Repository\IVideoTagRepository;
use Atom\Uploader\Model\Embeddable\FileReference;


/**
 * Class VideoHandler
 *
 * @author <thecold147@gmail.com>
 */
class VideoHandler
{
    private $userRepo;

    private $videoRepo;

    private $categoryRepo;

    private $tagRepo;

    public function __construct(
        IUserRepository $userRepo,
        IVideoRepository $videoRepo,
        IVideoCategoryRepository $categoryRepo,
        IVideoTagRepository $tagRepo
    ) {
        $this->userRepo = $userRepo;
        $this->videoRepo = $videoRepo;
        $this->categoryRepo = $categoryRepo;
        $this->tagRepo = $tagRepo;
    }

    public function createVideo(CreateVideoCommand $command)
    {
        $author = $this->userRepo->findOrFail($command->authorId);
        $category = $this->categoryRepo->findOrFail($command->categoryId);
        $video = new Video(
            new FileReference($command->original),
            $author,
            $command->title,
            $command->description,
            $command->isCommentable,
            $command->accessLevel ?: Video::ACCESS_LINK,
            $category,
            $this->prepareTags((array)$command->tags)
        );
        $this->videoRepo->add($video);
        return $video->toArray();
    }

    public function updateVideo(UpdateVideoCommand $command)
    {
        $video = $this->videoRepo->findOrFail($command->id);
        $this->deniedIfNotAuthor($video, $command->authorId);
        if ($video->getCategory()->getId() === (int)$command->categoryId) {
            $category = null;
        } else {
            $category = $this->categoryRepo->findOrFail($command->categoryId);
        }
        $video->update(
            $command->title,
            $command->description,
            $command->isCommentable,
            $command->accessLevel,
            $category,
            $this->prepareTags((array)$command->tags)
        );
        $this->videoRepo->add($video);
    }

    public function deleteVideo(DeleteVideoCommand $command)
    {
        $video = $this->videoRepo->findOrFail($command->id);
        $this->deniedIfNotAuthor($video, $command->authorId);
        $this->videoRepo->remove($video);
    }


    public function getVideoById(GetVideoByIdCommand $command)
    {
        $video = $this->videoRepo->findOrFail($command->id);
        return $video->toArray();
    }

    private function prepareTags(array $tags)
    {
        foreach ($tags as &$tag) {
            $tag = $this->tagRepo->findByName($tag) ?: new VideoTag($tag);
        }
        return $tags;
    }

    private function deniedIfNotAuthor(Video $video, $authorId)
    {
        if ($video->getAuthor()->getId() !== (int)$authorId) {
            throw new AccessDeniedException();
        }
    }
}
