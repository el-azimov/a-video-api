<?php

namespace App\Application\Handler;

use App\Application\Command\VideoVote\CreateOrUpdateVideoVoteCommand;
use App\Domain\Entity\VideoVote;
use App\Domain\Repository\IUserRepository;
use App\Domain\Repository\IVideoRepository;
use App\Domain\Repository\IVideoVoteRepository;


/**
 * Class VideoVoteHandler
 *
 * @author <thecold147@gmail.com>
 */
class VideoVoteHandler
{
    private $voteRepo;

    private $userRepo;

    private $videoRepo;

    public function __construct(IVideoVoteRepository $voteRepo, IUserRepository $userRepo, IVideoRepository $videoRepo)
    {
        $this->voteRepo = $voteRepo;
        $this->userRepo = $userRepo;
        $this->videoRepo = $videoRepo;
    }

    public function createOrUpdateVote(CreateOrUpdateVideoVoteCommand $command)
    {
        $author = $this->userRepo->findOrFail($command->authorId);
        $video = $this->videoRepo->findOrFail($command->videoId);
        $vote = $this->voteRepo->findByAuthorAndVideo($author, $video);
        if ($vote) {
            $vote->vote($command->vote);
        } else {
            $vote = new VideoVote($video, $author, $command->vote);
            $this->voteRepo->add($vote);
        }
    }
}
