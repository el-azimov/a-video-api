<?php

namespace App\Application\Handler;

use App\Application\Exception\BadCredentialsException;
use App\Application\Command\Security\LoginCommand;
use App\Application\Port\ITokenManager;
use App\Domain\Exception\NotFoundException;
use App\Domain\Repository\IUserRepository;


/**
 * Class SecurityHandler
 *
 * @author <thecold147@gmail.com>
 */
class SecurityHandler
{
    private $userRepository;

    private $tokenManager;

    public function __construct(IUserRepository $userRepository, ITokenManager $tokenManager)
    {
        $this->userRepository = $userRepository;
        $this->tokenManager = $tokenManager;
    }

    public function login(LoginCommand $command)
    {
        $user = $this->userRepository->findByUsernameOrEmail($command->usernameOrEmail);
        if (!$user) {
            throw new NotFoundException(sprintf('User with username "%s" not found', $command->usernameOrEmail));
        }
        if (!$user->isPasswordValid($command->password)) {
            throw new BadCredentialsException();
        };
        return ['token' => $this->tokenManager->create($user)];
    }
}
