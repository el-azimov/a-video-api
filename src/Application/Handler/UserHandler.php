<?php

namespace App\Application\Handler;

use App\Application\Command\User\DeleteUserByIdCommand;
use App\Application\Command\User\CreateUserCommand;
use App\Application\Command\User\DeleteUserByUsernameCommand;
use App\Application\Command\User\GetUserByIdCommand;
use App\Application\Command\User\GetUserByUsernameCommand;
use App\Application\Command\User\UpdateUserByIdCommand;
use App\Application\Command\User\UpdateUserByUsernameCommand;
use App\Application\Command\User\UpdateUserCommand;
use App\Domain\Entity\User;
use App\Domain\Repository\IUserRepository;


/**
 * Class UserHandler
 *
 * @author <thecold147@gmail.com>
 */
class UserHandler
{
    private $userRepo;

    public function __construct(IUserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function createUser(CreateUserCommand $command)
    {
        $user = new User(
            $command->username,
            new \DateTime($command->birthday),
            $command->password,
            $command->email,
            $command->gender,
            $command->firstName,
            $command->lastName
        );
        $user->activate();
        $this->userRepo->add($user);
        return $user->toArray();
    }

    public function getUserById(GetUserByIdCommand $command)
    {
        $user = $this->userRepo->findOrFail($command->id);
        return $user->toArray();
    }

    public function getUserByUsername(GetUserByUsernameCommand $command)
    {
        $user = $this->userRepo->findByUsernameOrFail($command->username);
        return $user->toArray();
    }

    public function deleteUserById(DeleteUserByIdCommand $command)
    {
        $user = $this->userRepo->findOrFail($command->id);
        $this->userRepo->remove($user);
    }

    public function deleteUserByUsername(DeleteUserByUsernameCommand $command)
    {
        $user = $this->userRepo->findByUsernameOrFail($command->username);
        $this->userRepo->remove($user);
    }

    public function updateUserById(UpdateUserByIdCommand $command)
    {
        $user = $this->userRepo->findOrFail($command->id);
        $this->updateUser($user, $command);
    }

    public function updateUserByUsername(UpdateUserByUsernameCommand $command)
    {
        $user = $this->userRepo->findByUsernameOrFail($command->currentUsername);
        $this->updateUser($user, $command);
    }

    private function updateUser(User $user, UpdateUserCommand $command)
    {
        $user->update(
            $command->username,
            new \DateTime($command->birthday),
            $command->password,
            $command->email,
            $command->gender,
            $command->firstName,
            $command->lastName
        );
    }
}
