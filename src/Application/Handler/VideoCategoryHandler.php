<?php

namespace App\Application\Handler;

use App\Application\Command\VideoCategory\CreateVideoCategoryCommand;
use App\Application\Command\VideoCategory\GetVideoCategoriesCommand;
use App\Application\Command\VideoCategory\GetVideoCategoryByIdCommand;
use App\Application\Command\VideoCategory\GetVideoCategoryByNameCommand;
use App\Application\Command\VideoCategory\UpdateVideoCategoryByIdCommand;
use App\Application\Command\VideoCategory\UpdateVideoCategoryByNameCommand;
use App\Domain\Entity\VideoCategory;
use App\Domain\Repository\IVideoCategoryRepository;

/**
 * Class VideoCategoryHandler
 *
 * @author <thecold147@gmail.com>
 */
class VideoCategoryHandler
{
    private $categoryRepo;

    public function __construct(IVideoCategoryRepository $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    public function createCategory(CreateVideoCategoryCommand $command)
    {
        $category = new VideoCategory($command->name);
        $this->categoryRepo->add($category);
        return $category->toArray();
    }

    public function getCategoryById(GetVideoCategoryByIdCommand $command)
    {
        $category = $this->categoryRepo->findOrFail($command->id);
        return $category->toArray();
    }

    public function getCategoryByName(GetVideoCategoryByNameCommand $command)
    {
        $category = $this->categoryRepo->findByNameOrFail($command->name);
        return $category->toArray();
    }

    public function updateCategoryById(UpdateVideoCategoryByIdCommand $command)
    {
        $category = $this->categoryRepo->findOrFail($command->id);
        $category->update($command->name);
    }

    public function updateCategoryByName(UpdateVideoCategoryByNameCommand $command)
    {
        $category = $this->categoryRepo->findByNameOrFail($command->currentName);
        $category->update($command->name);
    }

    public function getCategories(GetVideoCategoriesCommand $command)
    {
        $pageSize = $command->pageSize ?: 20;
        $categories = $this->categoryRepo->findAll($command->page, $pageSize);
        $items = array_map([$this, 'categoryToArray'], iterator_to_array($categories));
        $pagesCount = ceil($categories->count() / $pageSize);
        return compact('items', 'pagesCount');
    }

    private function categoryToArray(VideoCategory $category)
    {
        return $category->toArray();
    }
}
