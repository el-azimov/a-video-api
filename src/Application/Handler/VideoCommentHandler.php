<?php

namespace App\Application\Handler;

use App\Application\Command\VideoComment\CreateVideoCommentCommand;
use App\Application\Command\VideoComment\DeleteVideoCommentByIdCommand;
use App\Application\Command\VideoComment\GetVideoCommentByIdCommand;
use App\Application\Command\VideoComment\UpdateVideoCommentByIdCommand;
use App\Application\Exception\AccessDeniedException;
use App\Domain\Entity\VideoComment;
use App\Domain\Repository\IUserRepository;
use App\Domain\Repository\IVideoCommentRepository;
use App\Domain\Repository\IVideoRepository;


/**
 * Class VideoCommentHandler
 *
 * @author <thecold147@gmail.com>
 */
class VideoCommentHandler
{
    private $commentRepo;

    private $userRepo;

    private $videoRepo;

    public function __construct(
        IVideoCommentRepository $commentRepo,
        IUserRepository $userRepo,
        IVideoRepository $videoRepo
    ) {
        $this->commentRepo = $commentRepo;
        $this->userRepo = $userRepo;
        $this->videoRepo = $videoRepo;
    }

    public function createComment(CreateVideoCommentCommand $command)
    {
        $author = $this->userRepo->findOrFail($command->authorId);
        $video = $this->videoRepo->findOrFail($command->videoId);
        $comment = new VideoComment($command->message, $author, $video);
        $this->commentRepo->add($comment);
        return $comment->toArray();
    }

    public function updateCommentById(UpdateVideoCommentByIdCommand $command)
    {
        $comment = $this->commentRepo->findOrFail($command->id);
        if ($comment->getAuthor()->getId() !== (int)$command->authorId) {
            throw new AccessDeniedException();
        }
        $comment->update($command->message);
    }

    public function deleteCommentById(DeleteVideoCommentByIdCommand $command)
    {
        $comment = $this->commentRepo->findOrFail($command->id);
        if ($comment->getAuthor()->getId() !== (int)$command->authorId) {
            throw new AccessDeniedException();
        }
        $this->commentRepo->remove($comment);
    }

    public function getCommentById(GetVideoCommentByIdCommand $command)
    {
        return $this->commentRepo->findOrFail($command->id);
    }
}
