<?php

namespace App\Application\Command;

/**
 * Class PaginationCommand
 *
 * @author <thecold147@gmail.com>
 */
abstract class PaginationCommand
{
    public $page;

    public $pageSize;
}
