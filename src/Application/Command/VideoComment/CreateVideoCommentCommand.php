<?php

namespace App\Application\Command\VideoComment;


/**
 * Class CreateVideoCommentCommand
 *
 * @author <thecold147@gmail.com>
 */
class CreateVideoCommentCommand
{
    public $authorId;

    public $message;

    public $videoId;
}
