<?php

namespace App\Application\Command\VideoComment;


/**
 * Class UpdateVideoCommentByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class UpdateVideoCommentByIdCommand
{
    public $id;

    public $message;

    public $authorId;
}
