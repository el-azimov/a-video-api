<?php

namespace App\Application\Command\VideoComment;


/**
 * Class GetVideoCommentByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class GetVideoCommentByIdCommand
{
    public $id;
}
