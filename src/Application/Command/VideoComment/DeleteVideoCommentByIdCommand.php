<?php

namespace App\Application\Command\VideoComment;


/**
 * Class DeleteVideoCommentByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class DeleteVideoCommentByIdCommand
{
    public $id;

    public $authorId;
}
