<?php

namespace App\Application\Command\User;

/**
 * Class CreateUserCommand
 *
 * @author <thecold147@gmail.com>
 */
class CreateUserCommand
{
    public $username;

    public $password;

    public $email;

    public $gender;

    public $firstName;

    public $lastName;

    public $birthday;
}
