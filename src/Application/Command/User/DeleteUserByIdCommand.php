<?php

namespace App\Application\Command\User;

/**
 * Class DeleteUserByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class DeleteUserByIdCommand
{
    public $id;
}
