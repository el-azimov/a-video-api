<?php

namespace App\Application\Command\User;


/**
 * Class GetUserByUsernameCommand
 *
 * @author <thecold147@gmail.com>
 */
class GetUserByUsernameCommand
{
    public $username;
}
