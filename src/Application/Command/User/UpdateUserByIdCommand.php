<?php

namespace App\Application\Command\User;


/**
 * Class UpdateUserByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class UpdateUserByIdCommand extends UpdateUserCommand
{
    public $id;
}
