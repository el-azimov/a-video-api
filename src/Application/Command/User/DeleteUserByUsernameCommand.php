<?php

namespace App\Application\Command\User;

/**
 * Class DeleteUserByUsernameCommand
 *
 * @author <thecold147@gmail.com>
 */
class DeleteUserByUsernameCommand
{
    public $username;
}
