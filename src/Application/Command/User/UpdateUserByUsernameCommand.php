<?php

namespace App\Application\Command\User;


/**
 * Class UpdateUserByUsernameCommand
 *
 * @author <thecold147@gmail.com>
 */
class UpdateUserByUsernameCommand extends UpdateUserCommand
{
    public $currentUsername;
}
