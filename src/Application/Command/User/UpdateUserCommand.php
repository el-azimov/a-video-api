<?php

namespace App\Application\Command\User;

/**
 * Class UpdateUserCommand
 *
 * @author <thecold147@gmail.com>
 */
abstract class UpdateUserCommand
{
    public $username;

    public $password;

    public $email;

    public $gender;

    public $firstName;

    public $lastName;

    public $birthday;
}
