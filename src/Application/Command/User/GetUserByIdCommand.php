<?php

namespace App\Application\Command\User;


/**
 * Class GetUserByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class GetUserByIdCommand
{
    public $id;
}
