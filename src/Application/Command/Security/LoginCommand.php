<?php

namespace App\Application\Command\Security;

/**
 * Class LoginCommand
 *
 * @author <thecold147@gmail.com>
 */
class LoginCommand
{
    public $usernameOrEmail;

    public $password;
}
