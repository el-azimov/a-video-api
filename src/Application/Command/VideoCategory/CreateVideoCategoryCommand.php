<?php

namespace App\Application\Command\VideoCategory;

/**
 * Class CreateVideoCategoryCommand
 *
 * @author <thecold147@gmail.com>
 */
class CreateVideoCategoryCommand
{
    public $name;
}
