<?php

namespace App\Application\Command\VideoCategory;

/**
 * Class GetVideoCategoryByNameCommand
 *
 * @author <thecold147@gmail.com>
 */
class GetVideoCategoryByNameCommand
{
    public $name;
}
