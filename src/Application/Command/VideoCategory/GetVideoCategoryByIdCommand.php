<?php

namespace App\Application\Command\VideoCategory;

/**
 * Class GetVideoCategoryByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class GetVideoCategoryByIdCommand
{
    public $id;
}
