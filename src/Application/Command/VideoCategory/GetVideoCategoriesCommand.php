<?php

namespace App\Application\Command\VideoCategory;

use App\Application\Command\PaginationCommand;

/**
 * Class GetVideoCategoriesCommand
 *
 * @author <thecold147@gmail.com>
 */
class GetVideoCategoriesCommand extends PaginationCommand
{

}
