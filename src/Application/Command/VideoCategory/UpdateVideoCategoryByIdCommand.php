<?php

namespace App\Application\Command\VideoCategory;

/**
 * Class UpdateVideoCategoryByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class UpdateVideoCategoryByIdCommand
{
    public $id;

    public $name;
}
