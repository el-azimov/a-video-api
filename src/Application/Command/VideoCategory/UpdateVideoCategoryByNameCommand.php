<?php

namespace App\Application\Command\VideoCategory;

/**
 * Class UpdateVideoCategoryByNameCommand
 *
 * @author <thecold147@gmail.com>
 */
class UpdateVideoCategoryByNameCommand
{
    public $name;

    public $currentName;
}
