<?php

namespace App\Application\Command\VideoVote;


/**
 * Class CreateOrUpdateVideoVoteCommand
 *
 * @author <thecold147@gmail.com>
 */
class CreateOrUpdateVideoVoteCommand
{
    public $authorId;

    public $videoId;

    public $vote;
}
