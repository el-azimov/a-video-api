<?php

namespace App\Application\Command\VideoTag;


/**
 * Class UpdateVideoTagByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class UpdateVideoTagByIdCommand
{
    public $id;

    public $name;
}
