<?php

namespace App\Application\Command\VideoTag;


/**
 * Class GetVideoTagByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class GetVideoTagByIdCommand
{
    public $id;
}
