<?php

namespace App\Application\Command\VideoTag;


/**
 * Class DeleteVideoTagByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class DeleteVideoTagByIdCommand
{
    public $id;
}
