<?php

namespace App\Application\Command\Video;


/**
 * Class CreateVideoCommand
 *
 * @author <thecold147@gmail.com>
 */
class CreateVideoCommand
{
    public $title;

    public $description;

    public $authorId;

    public $original;

    public $categoryId;

    public $isCommentable;

    public $accessLevel;

    public $tags;
}
