<?php

namespace App\Application\Command\Video;


/**
 * Class GetVideoByIdCommand
 *
 * @author <thecold147@gmail.com>
 */
class GetVideoByIdCommand
{
    public $id;
}
