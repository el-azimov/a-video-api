<?php

namespace App\Application\Command\Video;


/**
 * Class DeleteVideoCommand
 *
 * @author <thecold147@gmail.com>
 */
class DeleteVideoCommand
{
    public $id;

    public $authorId;
}
