<?php

namespace App\Application\Command\Video;


/**
 * Class UpdateVideoCommand
 *
 * @author <thecold147@gmail.com>
 */
class UpdateVideoCommand
{
    public $id;

    public $title;

    public $description;

    public $accessLevel;

    public $isCommentable;

    public $categoryId;

    public $authorId;

    public $tags;
}
