<?php

namespace App\Application\Exception;

use Exception;


/**
 * Class AccessDeniedException
 *
 * @author <thecold147@gmail.com>
 */
class AccessDeniedException extends \UnexpectedValueException
{
    public function __construct($message = 'Access denied.', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}
