<?php

namespace App\Application\Exception;

use Exception;


/**
 * Class HandlerNotImplementedException
 *
 * @author <thecold147@gmail.com>
 */
class HandlerNotImplementedException extends \LogicException
{
    public $data;

    public function __construct(
        $message = 'Handler for this command is not implemented.',
        $data = null,
        $code = 0,
        Exception $previous = null
    ) {
        $this->data = $data;
        parent::__construct($message, $code, $previous);
    }

}
