<?php

namespace App\Application\Exception;

use Exception;


/**
 * Class BadCredentialsException
 *
 * @author <thecold147@gmail.com>
 */
class BadCredentialsException extends BadRequestException
{
    public function __construct($message = 'Invalid credentials.', $data = null, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $data, $code, $previous);
    }
}
