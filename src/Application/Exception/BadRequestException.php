<?php

namespace App\Application\Exception;


/**
 * Class BadRequestException
 *
 * @author <thecold147@gmail.com>
 */
class BadRequestException extends \RuntimeException
{
    public $data;

    public function __construct(
        $message = 'This value is not a valid command.',
        $data = null,
        $code = 0,
        \Exception $previous = null
    ) {
        $this->data = $data;
        parent::__construct($message, $code, $previous);
    }
}
