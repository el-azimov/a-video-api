<?php

namespace App\Application\Adapter;

use App\Application\Exception\AccessDeniedException;
use App\Application\Exception\BadRequestException;
use App\Application\Port\IAuthorizeChecker;
use App\Application\Port\ICommandValidator;
use App\Application\Port\IHandleResolver;
use App\Application\Port\ICommandBus;


/**
 * Class SecurityCommandBus
 *
 * @author <thecold147@gmail.com>
 */
class SecurityCommandBus implements ICommandBus
{
    private $validator;

    private $resolver;

    private $authorizeChecker;

    public function __construct(
        ICommandValidator $validator,
        IHandleResolver $resolver,
        IAuthorizeChecker $authorizeChecker
    ) {
        $this->validator = $validator;
        $this->resolver = $resolver;
        $this->authorizeChecker = $authorizeChecker;
    }

    /**
     * @param $command
     *
     * @return mixed
     */
    public function execute($command)
    {
        if (!$this->resolver->isSupportsCommand($command)) {
            throw new BadRequestException();
        }
        if (!$this->authorizeChecker->isGranted($command)) {
            throw new AccessDeniedException();
        }
        $this->validator->validate($command);
        return call_user_func($this->resolver->resolve($command), $command);
    }
}
