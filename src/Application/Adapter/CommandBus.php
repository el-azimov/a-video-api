<?php

namespace App\Application\Adapter;

use App\Application\Port\IHandleResolver;
use App\Application\Port\ICommandBus;


/**
 * Class CommandBus
 *
 * @author <thecold147@gmail.com>
 */
class CommandBus implements ICommandBus
{
    private $handleResolver;

    public function __construct(IHandleResolver $handleResolver)
    {
        $this->handleResolver = $handleResolver;
    }


    public function execute($command)
    {
        return call_user_func($this->handleResolver->resolve($command), $command);
    }
}
