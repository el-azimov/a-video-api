<?php

namespace App\Application\Adapter;

use App\Application\Port\ICommandValidator;
use App\Application\Port\IHandleResolver;
use App\Application\Port\ICommandBus;


/**
 * Class ValidateCommandBus
 *
 * @author <thecold147@gmail.com>
 */
class ValidateCommandBus implements ICommandBus
{
    private $handleResolver;

    private $validator;

    public function __construct(IHandleResolver $handleResolver, ICommandValidator $validator)
    {
        $this->handleResolver = $handleResolver;
        $this->validator = $validator;
    }


    /**
     * @param $command
     *
     * @return mixed
     */
    public function execute($command)
    {
        $this->validator->validate($command);
        return call_user_func($this->handleResolver->resolve($command), $command);
    }
}
