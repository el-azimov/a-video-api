<?php

namespace App\Api\Middleware;

use App\Application\Port\IHandleResolver;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class RequestDTOConverter
 *
 * @author <thecold147@gmail.com>
 */
class RequestDTOConverter implements ParamConverterInterface
{
    private $serializer;

    private $validator;

    private $handleResolver;

    public function __construct(Serializer $serializer, ValidatorInterface $validator, IHandleResolver $handleResolver)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->handleResolver = $handleResolver;
    }


    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $request->setRequestFormat('json');
        if (in_array($request->getMethod(), ['POST', 'PUT'])) {
            $content = $request->getContent();
            if ($content) {
                $userData = array_merge(
                    (array)json_decode($content, true),
                    $request->request->all(),
                    $request->attributes->all()
                );
            } else {
                $userData = array_merge($request->request->all(), $request->attributes->all(), $request->files->all());
            }
        }
        if (!isset($userData)) {
            $userData = array_merge($request->query->all(), $request->attributes->all());
        }

        $request->attributes->set(
            $configuration->getName(),
            $this->serializer->denormalize($userData, $configuration->getClass())
        );
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return $this->handleResolver->isSupportsCommand($configuration->getClass());
    }
}
