<?php

namespace App\Api\Middleware;

use App\Application\Port\IHandleResolver;
use App\Application\Port\ICommandBus;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ResponseListener
 *
 * @author <thecold147@gmail.com>
 */
class ResponseListener
{
    private $manager;

    private $serializer;

    private $app;

    private $handleResolver;

    public function __construct(
        ObjectManager $manager,
        Serializer $serializer,
        ICommandBus $app,
        IHandleResolver $handleResolver
    )
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
        $this->app = $app;
        $this->handleResolver = $handleResolver;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($event->isMasterRequest()) {
            $this->manager->flush();
        };
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $controllerResult = $event->getControllerResult();

        if (is_object($controllerResult) && $this->handleResolver->isSupportsCommand($controllerResult)) {
            $event->setControllerResult($this->app->execute($controllerResult));
        }

        $request = $event->getRequest();
        $result = $event->getControllerResult();

        if (null !== $result) {
            $result = $this->serializer->serialize($result, 'json');
        }


        switch ($request->getMethod()) {
            case Request::METHOD_POST:
                $statusCode = Response::HTTP_CREATED;
                break;
            case Request::METHOD_GET:
                $statusCode = Response::HTTP_OK;
                break;
            default:
                $statusCode = Response::HTTP_NO_CONTENT;
        }
        $event->setResponse(new Response($result, $statusCode));
    }
}
