<?php
/**
 * Created by IntelliJ IDEA.
 * User: cold
 * Date: 10/13/15
 * Time: 3:15 AM
 */

namespace App\Api\Security;

use App\Domain\Entity\User;
use App\Domain\Repository\IUserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    private $userRepository;

    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /** {@inheritdoc} */
    public function loadUserByUsername($username)
    {
        if ($user = $this->userRepository->findByUsernameOrEmail($username)) {
            return $user;
        }
        throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
    }

    /** {@inheritdoc} */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }
        $this->userRepository->refresh($user);
        return $user;
    }

    /** {@inheritdoc} */
    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
