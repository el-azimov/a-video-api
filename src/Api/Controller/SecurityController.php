<?php

namespace App\Api\Controller;

use App\Application\Command\Security\LoginCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SecurityController
 *
 * @author <thecold147@gmail.com>
 */
class SecurityController
{
    /**
     * @Route(path="/login", methods={"POST"})
     */
    public function loginAction(LoginCommand $command)
    {
        return $command;
    }


    /**
     * @Route(path="/security/is-authenticated-fully", methods={"GET", "HEAD"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function secureAction()
    {
    }
}
