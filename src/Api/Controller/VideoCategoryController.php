<?php

namespace App\Api\Controller;

use App\Application\Command\VideoCategory\CreateVideoCategoryCommand;
use App\Application\Command\VideoCategory\GetVideoCategoriesCommand;
use App\Application\Command\VideoCategory\GetVideoCategoryByIdCommand;
use App\Application\Command\VideoCategory\GetVideoCategoryByNameCommand;
use App\Application\Command\VideoCategory\UpdateVideoCategoryByIdCommand;
use App\Application\Command\VideoCategory\UpdateVideoCategoryByNameCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class VideoCategoryController
 *
 * @author <thecold147@gmail.com>
 */
class VideoCategoryController
{
    /**
     * @Route(path="/videos/categories", methods={"POST"})
     */
    public function createCategoryAction(CreateVideoCategoryCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/videos/categories/{id}", requirements={"id": "\d+"}, methods={"GET"})
     */
    public function getCategoryByIdAction($id)
    {
        $command = new GetVideoCategoryByIdCommand();
        $command->id = $id;
        return $command;
    }

    /**
     * @Route(path="/videos/categories/{name}", methods={"GET"})
     */
    public function getCategoryByNameAction($name)
    {
        $command = new GetVideoCategoryByNameCommand();
        $command->name = $name;
        return $command;
    }

    /**
     * @Route(path="/videos/categories", methods={"GET"})
     */
    public function getCategoriesAction(GetVideoCategoriesCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/videos/categories/{id}", methods={"PUT"}, requirements={"id": "\d+"})
     */
    public function updateCategoryByIdAction(UpdateVideoCategoryByIdCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/videos/categories/{currentName}", requirements={"currentName": ".*[a-zA-Z]+.*"}, methods={"PUT"})
     */
    public function updateCategoryByNameAction(UpdateVideoCategoryByNameCommand $command)
    {
        return $command;
    }
}
