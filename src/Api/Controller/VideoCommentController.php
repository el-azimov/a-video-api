<?php

namespace App\Api\Controller;

use App\Application\Command\VideoComment\CreateVideoCommentCommand;
use App\Application\Command\VideoComment\DeleteVideoCommentByIdCommand;
use App\Application\Command\VideoComment\GetVideoCommentByIdCommand;
use App\Application\Command\VideoComment\UpdateVideoCommentByIdCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class VideoCommentController
 *
 * @author <thecold147@gmail.com>
 */
class VideoCommentController extends Controller
{
    /**
     * @Route(path="/videos/{videoId}/comments", requirements={"videoId": "\d+"}, methods={"POST"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function createCommentAction(CreateVideoCommentCommand $command)
    {
        $command->authorId = $command->authorId ?: $this->getUserId();
        return $command;
    }

    /**
     * @Route(path="/comments/{id}", requirements={"id": "\d+"}, methods={"PUT"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function updateCommentByIdAction(UpdateVideoCommentByIdCommand $command)
    {
        $command->authorId = $command->authorId ?: $this->getUserId();
        return $command;
    }

    /**
     * @Route(path="/comments/{id}", requirements={"id": "\d+"}, methods={"DELETE"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function deleteCommentByIdAction(DeleteVideoCommentByIdCommand $command)
    {
        $command->authorId = $command->authorId ?: $this->getUserId();
        return $command;
    }

    /**
     * @Route(path="/comments/{id}", requirements={"id": "\d+"}, methods={"GET", "HEAD"})
     */
    public function getCommentByIdAction(GetVideoCommentByIdCommand $command)
    {
        return $command;
    }
}
