<?php

namespace App\Api\Controller;

use App\Application\Command\VideoVote\CreateOrUpdateVideoVoteCommand;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class VideoVoteController
 *
 * @author <thecold147@gmail.com>
 */
class VideoVoteController extends Controller
{
    /**
     * @Route(path="/videos/{videoId}/vote", requirements={"videoId": "\d+"}, methods={"PUT"})
     */
    public function createOrUpdateVote(CreateOrUpdateVideoVoteCommand $command)
    {
        $command->authorId = $command->authorId ?: $this->getUserId();
        return $command;
    }
}
