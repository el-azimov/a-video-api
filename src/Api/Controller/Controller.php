<?php

namespace App\Api\Controller;

use App\Domain\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller
 *
 * @method User|null getUser
 * @author <thecold147@gmail.com>
 */
abstract class Controller extends BaseController
{
    protected function execute($command)
    {
        return $this->get('api.app_adapter')->execute($command);
    }

    protected function view($data, $statusCode = Response::HTTP_OK, $context = [], $format = 'json', $headers = [])
    {
        if (is_object($data) && $this->get('infra.handle_resolver')->isSupportsCommand($data)) {
            $data = $this->execute($data);
        }
        return new Response($this->get('serializer')->serialize($data, $format, $context), $statusCode, $headers);
    }

    protected function setLocation($routeName, array $parameters = [])
    {
        $this->get('request')->headers->set('location', $this->get('router')->generate($routeName, $parameters));
    }

    protected function getUserId()
    {
        $user = $this->getUser();
        if ($user instanceof User) {
            return $user->getId();
        }
        return null;
    }
}
