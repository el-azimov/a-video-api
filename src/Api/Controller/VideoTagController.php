<?php

namespace App\Api\Controller;

use App\Application\Command\VideoTag\CreateVideoTagCommand;
use App\Application\Command\VideoTag\DeleteVideoTagByIdCommand;
use App\Application\Command\VideoTag\GetVideoTagByIdCommand;
use App\Application\Command\VideoTag\GetVideoTagsCommand;
use App\Application\Command\VideoTag\UpdateVideoTagByIdCommand;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class VideoTagController
 *
 * @author <thecold147@gmail.com>
 */
class VideoTagController
{
    /**
     * @Route(path="/tags", methods={"POST"})
     */
    public function createTagAction(CreateVideoTagCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/tags/{id}", requirements={"id": "\d+"}, methods={"PUT"})
     */
    public function updateTagAction(UpdateVideoTagByIdCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/tags/{id}", requirements={"id": "\d+"}, methods={"GET"})
     */
    public function getTagById(GetVideoTagByIdCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/tags/{id}", requirements={"id": "\d+"}, methods={"DELETE"})
     */
    public function deleteTagById(DeleteVideoTagByIdCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/tags", methods={"GET"})
     */
    public function getTags(GetVideoTagsCommand $command)
    {
        return $command;
    }
}
