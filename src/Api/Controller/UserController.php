<?php

namespace App\Api\Controller;


use App\Application\Command\User\DeleteUserByIdCommand;
use App\Application\Command\User\CreateUserCommand;
use App\Application\Command\User\DeleteUserByUsernameCommand;
use App\Application\Command\User\GetUserByIdCommand;
use App\Application\Command\User\GetUserByUsernameCommand;
use App\Application\Command\User\UpdateUserByIdCommand;
use App\Application\Command\User\UpdateUserByUsernameCommand;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @author <thecold147@gmail.com>
 */
class UserController
{
    /**
     * @Route(path="/users", methods={"POST"})
     */
    public function createUserAction(CreateUserCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/users/{id}", requirements={"id": "\d+"}, methods={"GET"})
     */
    public function getUserAction(GetUserByIdCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/users/{username}", requirements={"username": ".*[a-zA-Z]+.*"}, methods={"GET"})
     */
    public function getUserByUsernameAction(GetUserByUsernameCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/users/{id}", methods={"DELETE"}, requirements={"id": "\d+"})
     */
    public function deleteUserByIdAction(DeleteUserByIdCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/users/{username}", requirements={"username": ".*[a-zA-Z]+.*"}, methods={"DELETE"})
     */
    public function deleteUserByUsernameAction(DeleteUserByUsernameCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/users/{id}", requirements={"id": "\d+"}, methods={"PUT"})
     */
    public function updateUserByIdAction(UpdateUserByIdCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/users/{currentUsername}", requirements={"currentUsername": ".*[a-zA-Z]+.*"}, methods={"PUT"})
     */
    public function updateUserByUsernameAction(UpdateUserByUsernameCommand $command)
    {
        return $command;
    }
}
