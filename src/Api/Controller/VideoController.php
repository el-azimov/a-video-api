<?php

namespace App\Api\Controller;

use App\Application\Command\Video\CreateVideoCommand;
use App\Application\Command\Video\DeleteVideoCommand;
use App\Application\Command\Video\GetVideoByIdCommand;
use App\Application\Command\Video\UpdateVideoCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class VideoController
 *
 * @author <thecold147@gmail.com>
 */
class VideoController extends Controller
{
    /**
     * @Route(path="/videos", methods={"POST"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function createVideoAction(CreateVideoCommand $command, Request $request)
    {
        $command->authorId = $command->authorId ?: $this->getUserId();
        $command->isCommentable = (bool)$request->get('isCommentable', true);
        return $command;
    }

    /**
     * @Route(path="/videos/{id}", requirements={"id": "\d+"}, methods={"PUT"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function updateVideo(UpdateVideoCommand $command)
    {
        $command->authorId = $command->authorId ?: $this->getUserId();
        return $command;
    }


    /**
     * @Route(path="/videos/{id}", requirements={"id": "\d+"}, methods={"DELETE"})
     */
    public function deleteVideo(DeleteVideoCommand $command)
    {
        $command->authorId = $command->authorId ?: $this->getUserId();
        return $command;
    }

    /**
     * @Route(path="/videos/{id}", requirements={"id": "\d+"}, methods={"GET"})
     */
    public function getVideoById(GetVideoByIdCommand $command)
    {
        return $command;
    }
}
