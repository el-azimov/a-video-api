<?php

namespace App\Api\Adapter;

use App\Application\Exception\AccessDeniedException;
use App\Application\Exception\BadRequestException;
use App\Domain\Exception\NotFoundException;
use App\Application\Port\ICommandBus;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * Class AppAdapter
 *
 * @author <thecold147@gmail.com>
 */
class AppAdapter implements ICommandBus
{
    private $commandBus;

    private $serializer;

    public function __construct(ICommandBus $commandBus, SerializerInterface $serializer)
    {
        $this->commandBus = $commandBus;
        $this->serializer = $serializer;
    }

    public function execute($command)
    {
        try {
            return $this->commandBus->execute($command);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        } catch (BadRequestException $e) {
            if ($e->data) {
                $message = $this->serializer->serialize($e->data, 'json');
            } else {
                $message = $e->getMessage();
            }
            throw new BadRequestHttpException($message, $e);
        } catch (AccessDeniedException $e) {
            throw new AccessDeniedHttpException($e->getMessage(), $e);
        }
    }
}
