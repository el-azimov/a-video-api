<?php

namespace App\Infrastructure\Adapter;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;


/**
 * Class HandleResolverCompilePass
 *
 * @author <thecold147@gmail.com>
 */
class HandleResolverCompilePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('infra.handle_resolver');
        $taggedService = $container->findTaggedServiceIds('infra.handle_resolver');
        $handlers = [];
        foreach ($taggedService as $id => $tags) {
            $reflection = new \ReflectionClass($container->findDefinition($id)->getClass());
            foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
                if (1 === $method->getNumberOfParameters()) {
                    $parameterClass = $method->getParameters()[0]->getClass();
                    $handlers[$parameterClass->getName()] = [$id, $method->getName()];

                }
            }
        }
        if (!empty($handlers)) {
            $definition->addMethodCall('setHandlers', [$handlers]);
        }
    }
}
