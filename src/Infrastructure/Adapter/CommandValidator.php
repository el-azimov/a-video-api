<?php

namespace App\Infrastructure\Adapter;

use App\Application\Exception\BadRequestException;
use App\Application\Port\ICommandValidator;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Class CommandValidator
 *
 * @author <thecold147@gmail.com>
 */
class CommandValidator implements ICommandValidator
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }


    /**
     * @param $command
     *
     * @throws BadRequestException
     */
    public function validate($command)
    {
        $validationErrors = $this->validator->validate($command);
        if (0 !== $validationErrors->count()) {
            $errorData = [];
            /** @var  ConstraintViolation $value */
            foreach ($validationErrors as $value) {
                $errorData[] = [
                    'message' => $value->getMessage(),
                    'propertyPath' => $value->getPropertyPath(),
                ];
            }
            throw new BadRequestException(null, $errorData);
        }
    }
}
