<?php

namespace App\Infrastructure\Adapter;

use App\Application\Port\ITokenManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Class TokenManager
 *
 * @author <thecold147@gmail.com>
 */
class TokenManager implements ITokenManager
{
    private $JWTManager;

    public function __construct(JWTManagerInterface $JWTManager)
    {
        $this->JWTManager = $JWTManager;
    }


    /**
     * @param UserInterface $user
     *
     * @return string
     */
    public function create(UserInterface $user)
    {
        return $this->JWTManager->create($user);
    }
}
