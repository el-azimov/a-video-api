<?php

namespace App\Infrastructure\Adapter;

use App\Application\Port\IAuthorizeChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


/**
 * Class AuthorizeChecker
 *
 * @author <thecold147@gmail.com>
 */
class AuthorizeChecker implements IAuthorizeChecker
{
    private $authorizeChecker;

    public function __construct(AuthorizationCheckerInterface $authorizeChecker)
    {
        $this->authorizeChecker = $authorizeChecker;
    }

    /**
     * Checks permissions to execute a command
     *
     * @param $command
     *
     * @return bool
     */
    public function isGranted($command)
    {
        return $this->authorizeChecker->isGranted(['execute'], $command);
    }
}
