<?php

namespace App\Infrastructure\Adapter;

use App\Application\Exception\HandlerNotImplementedException;
use App\Application\Port\IHandleResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class HandleResolver
 *
 * @author <thecold147@gmail.com>
 */
class HandleResolver implements IHandleResolver
{
    private $container;

    private $handlers;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function resolve($command)
    {
        $className = $this->toClassName($command);
        try {
            list($service, $method) = $this->handlers[$className];
            $handler = $this->container->get($service);
        } catch (\Exception $e) {
            throw new HandlerNotImplementedException();
        }
        return [$handler, $method];
    }

    public function isSupportsCommand($command)
    {
        return array_key_exists($this->toClassName($command), $this->handlers);
    }

    /**
     * @param array $handlers e.g [SomeCommand:class => ['some_service', 'method']]
     */
    public function setHandlers(array $handlers)
    {
        $this->handlers = $handlers;
    }

    private function toClassName($object)
    {
        return is_object($object) ? get_class($object) : $object;
    }
}
