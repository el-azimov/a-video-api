<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\VideoCategory;
use App\Domain\Exception\NotFoundException;
use App\Domain\Repository\IVideoCategoryRepository;

/**
 * Class VideoCategoryRepository
 *
 * @author <thecold147@gmail.com>
 */
class VideoCategoryRepository extends AbstractRepository implements IVideoCategoryRepository
{
    /**
     * @param int $id
     *
     * @return VideoCategory
     */
    public function find($id)
    {
        return $this->em->find(VideoCategory::class, $id);
    }

    /**
     * @param string $name
     *
     * @return VideoCategory|null
     */
    public function findByName($name)
    {
        return $this->createQueryBuilder(VideoCategory::class, 'c')
            ->where('LOWER(c.name) = LOWER(:name)')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAll($page, $pageSize)
    {
        $query = $this->createQueryBuilder(VideoCategory::class, 'c')->getQuery();
        return $this->createPagination($query, $page, $pageSize);
    }

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return VideoCategory
     */
    public function findOrFail($id)
    {
        return $this->failOnNull($this->find($id), sprintf('Video category with ID "%s" not found.', $id));
    }

    /**
     * @param $name
     *
     * @throws NotFoundException
     * @return VideoCategory
     */
    public function findByNameOrFail($name)
    {
        return $this->failOnNull($this->findByName($name), sprintf('Video category with name "%s" not found.', $name));
    }
}
