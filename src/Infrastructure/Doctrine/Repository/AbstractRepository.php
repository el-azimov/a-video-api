<?php

namespace App\Infrastructure\Doctrine\Repository;


use App\Domain\Exception\NotFoundException;
use App\Domain\Repository\IRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class AbstractRepository
 *
 * @author <thecold147@gmail.com>
 */
class AbstractRepository implements IRepository
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function add($entity)
    {
        $this->em->persist($entity);
    }

    public function remove($entity)
    {
        $this->em->remove($entity);
    }

    protected function createQueryBuilder($entityName, $alias, $indexBy = null)
    {
        return $this->em->createQueryBuilder()
            ->select($alias)
            ->from($entityName, $alias, $indexBy);
    }

    public function refresh($entity)
    {
        $this->em->refresh($entity);
    }

    protected function createPagination(Query $query, $page, $pageSize)
    {
        $query->setFirstResult($pageSize * max(0, $page - 1));
        $query->setMaxResults($pageSize);
        return new Paginator($query);
    }

    protected function failOnNull($object, $message)
    {
        if (null === $object) {
            throw new NotFoundException($message);
        }
        return $object;
    }
}
