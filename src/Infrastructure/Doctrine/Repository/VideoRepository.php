<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\Video;
use App\Domain\Exception\NotFoundException;
use App\Domain\Repository\IVideoRepository;


/**
 * Class VideoRepository
 *
 * @author <thecold147@gmail.com>
 */
class VideoRepository extends AbstractRepository implements IVideoRepository
{

    /**
     * @param $id
     *
     * @return Video|null
     */
    public function find($id)
    {
        return $this->em->find(Video::class, $id);
    }

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return Video
     */
    public function findOrFail($id)
    {
        return $this->failOnNull($video = $this->find($id), sprintf('Video with ID "%s" not found.', $id));
    }
}
