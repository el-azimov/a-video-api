<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\User;
use App\Domain\Entity\Video;
use App\Domain\Entity\VideoVote;
use App\Domain\Exception\NotFoundException;
use App\Domain\Repository\IVideoVoteRepository;


/**
 * Class VideoVoteRepository
 *
 * @author <thecold147@gmail.com>
 */
class VideoVoteRepository extends AbstractRepository implements IVideoVoteRepository
{

    /**
     * @param User $user
     *
     * @return VideoVote|null
     */
    public function findByAuthorAndVideo(User $author, Video $video)
    {
        return $this->createQueryBuilder(VideoVote::class, 'v')
            ->where('v.author = :author')
            ->andWhere('v.video = :video')
            ->setParameters(compact('author', 'video'))
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param User $author
     *
     * @throws NotFoundException
     * @return VideoVote
     */
    public function findByAuthorAndVideoOrFail(User $author, Video $video)
    {
        return $this->failOnNull($this->findByAuthorAndVideo($author, $video), 'Vote not found.');
    }
}
