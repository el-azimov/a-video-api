<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\User;
use App\Domain\Exception\NotFoundException;
use App\Domain\Repository\IUserRepository;

/**
 * Class UserRepository
 *
 * @author <thecold147@gmail.com>
 */
final class UserRepository extends AbstractRepository implements IUserRepository
{

    /**
     * @param string $usernameOrEmail
     *
     * @return User
     */
    public function findByUsernameOrEmail($usernameOrEmail)
    {
        if (filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL)) {
            return $this->findByEmail($usernameOrEmail);
        } else {
            return $this->findByUsername($usernameOrEmail);
        }
    }

    /**
     * @param string $id
     *
     * @return User
     */
    public function find($id)
    {
        return $this->filterByActive()
            ->andWhere('u.id = :id')
            ->setParameter(':id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $username
     *
     * @return User
     */
    public function findByUsername($username)
    {
        return $this->filterByActive()
            ->andWhere('LOWER(u.username) = LOWER(:username)')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * @param string $email
     *
     * @return User
     */
    public function findByEmail($email)
    {
        return $this->filterByActive()
            ->andWhere('LOWER(u.email) = LOWER(:email)')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return User[]
     */
    public function findAll()
    {
        return $this->filterByActive()->getQuery()->getResult();
    }

    private function filterByActive()
    {
        return $this->createQueryBuilder(User::class, 'u')
            ->where('u.status = :status')
            ->setParameter(':status', User::STATUS_ACTIVE);
    }

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return User
     */
    public function findOrFail($id)
    {
        return $this->failOnNull($user = $this->find($id), sprintf('User with ID "%s" not found.', $id));
    }

    /**
     * @param $username
     *
     * @throws NotFoundException
     * @return User
     */
    public function findByUsernameOrFail($username)
    {
        return $this->failOnNull(
            $this->findByUsername($username),
            sprintf('User with username "%s" not found.', $username)
        );
    }

    /**
     * @param $email
     *
     * @throws NotFoundException
     * @return User
     */
    public function findByEmailOrFail($email)
    {
        return $this->failOnNull(
            $user = $this->findByEmail($email),
            sprintf('User with email "%s" not found.', $email)
        );
    }

    /**
     * @param $usernameOrEmail
     *
     * @throws NotFoundException
     * @return User
     */
    public function findByUsernameOrEmailOrFail($usernameOrEmail)
    {
        if (filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL)) {
            return $this->findByUsernameOrFail($usernameOrEmail);
        } else {
            return $this->findByEmailOrFail($usernameOrEmail);
        }
    }
}
