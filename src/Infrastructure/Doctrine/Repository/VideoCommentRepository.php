<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\VideoComment;
use App\Domain\Exception\NotFoundException;
use App\Domain\Repository\IVideoCommentRepository;


/**
 * Class VideoCommentRepository
 *
 * @author <thecold147@gmail.com>
 */
class VideoCommentRepository extends AbstractRepository implements IVideoCommentRepository
{

    /**
     * @param $id
     *
     * @return VideoComment|null
     */
    public function find($id)
    {
        return $this->em->find(VideoComment::class, $id);
    }

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return VideoComment
     */
    public function findOrFail($id)
    {
        return $this->failOnNull($this->find($id), sprintf('Comment with ID "%s" not found.', $id));
    }
}
