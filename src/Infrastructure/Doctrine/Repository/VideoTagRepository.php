<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\VideoTag;
use App\Domain\Exception\NotFoundException;
use App\Domain\Repository\IVideoTagRepository;

/**
 * Class VideoTagRepository
 *
 * @author <thecold147@gmail.com>
 */
class VideoTagRepository extends AbstractRepository implements IVideoTagRepository
{

    /**
     * @param $id
     *
     * @return VideoTag|null
     */
    public function find($id)
    {
        return $this->em->find(VideoTag::class, $id);
    }

    /**
     * @param $id
     *
     * @throws NotFoundException
     * @return VideoTag
     */
    public function findOrFail($id)
    {
        return $this->failOnNull($tag = $this->find($id), sprintf('Video tag with ID "%s" not found.', $id));
    }

    /**
     * @param int $page
     * @param int $pageSize
     *
     * @return \IteratorAggregate|\Countable
     */
    public function findAll($page, $pageSize)
    {
        $query = $this->createQueryBuilder(VideoTag::class, 't')->getQuery();
        return $this->createPagination($query, $page, $pageSize);
    }

    /**
     * @param string $name
     *
     * @return VideoTag|null
     */
    public function findByName($name)
    {
        return $this->createQueryBuilder(VideoTag::class, 't')
            ->where('LOWER(t.name) = LOWER(:name)')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $name
     *
     * @throws NotFoundException
     * @return VideoTag
     */
    public function findByNameOrFail($name)
    {
        return $this->failOnNull($tag = $this->findByName($name), sprintf('Video Tag with ID "%s" not found.'));
    }
}
