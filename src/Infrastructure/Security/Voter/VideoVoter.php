<?php

namespace App\Infrastructure\Security\Voter;

use App\Application\Command\Video\CreateVideoCommand;
use App\Application\Command\Video\DeleteVideoCommand;
use App\Application\Command\Video\GetVideoByIdCommand;
use App\Application\Command\Video\UpdateVideoCommand;
use App\Domain\Entity\User;

/**
 * Class VideoVoter
 *
 * @author <thecold147@gmail.com>
 */
class VideoVoter extends AbstractVoter
{
    public function supportsClass($class)
    {
        return in_array($class,
            [
                CreateVideoCommand::class,
                UpdateVideoCommand::class,
                DeleteVideoCommand::class,
                GetVideoByIdCommand::class,
            ]);
    }

    public function isGranted($user, $className, $object)
    {
        switch ($className) {
            case CreateVideoCommand::class:
            case UpdateVideoCommand::class:
            case DeleteVideoCommand::class:
                return $user instanceof User && $user->getId() === (int)$object->authorId;
            default:
                return true;
        }
    }
}
