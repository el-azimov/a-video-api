<?php

namespace App\Infrastructure\Security\Voter;

use App\Application\Command\User\DeleteUserByIdCommand;
use App\Application\Command\User\CreateUserCommand;
use App\Application\Command\User\DeleteUserByUsernameCommand;
use App\Application\Command\User\GetUserByIdCommand;
use App\Application\Command\User\GetUserByUsernameCommand;
use App\Application\Command\User\UpdateUserByIdCommand;
use App\Application\Command\User\UpdateUserByUsernameCommand;
use App\Domain\Entity\User;

/**
 * Class UserVoter
 *
 * @author <thecold147@gmail.com>
 */
class UserVoter extends AbstractVoter
{
    public function supportsClass($class)
    {
        return in_array($class,
            [
                CreateUserCommand::class,
                UpdateUserByIdCommand::class,
                UpdateUserByUsernameCommand::class,
                DeleteUserByUsernameCommand::class,
                DeleteUserByIdCommand::class,
                GetUserByUsernameCommand::class,
                GetUserByIdCommand::class,
            ]);
    }

    public function isGranted($user, $className, $object)
    {
        switch ($className) {
            case UpdateUserByUsernameCommand::class:
                return $user instanceof User && $user->getUsername() === $object->currentUsername;
            case DeleteUserByUsernameCommand::class:
                return $user instanceof User && $user->getUsername() === $object->username;
            case DeleteUserByIdCommand::class:
            case UpdateUserByIdCommand::class:
                return $user instanceof User && $user->getId() === (int)$object->id;
            default:
                return true;
        }
    }
}
