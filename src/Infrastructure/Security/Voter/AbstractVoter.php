<?php

namespace App\Infrastructure\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;


/**
 * Class AbstractVoter
 *
 * @author <thecold147@gmail.com>
 */
abstract class AbstractVoter implements VoterInterface
{

    public function supportsAttribute($attribute)
    {
        return 'execute' === $attribute;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        $className = get_class($object);
        if (!$this->supportsClass($className)) {
            return self::ACCESS_ABSTAIN;
        }
        $vote = $this->isGranted($token->getUser(), $className, $object);
        if (is_bool($vote)) {
            return $vote ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
        } else {
            return $vote;
        }
    }

    abstract public function isGranted($user, $className, $object);
}
