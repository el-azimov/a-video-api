<?php

namespace App\Infrastructure\Security\Voter;

use App\Application\Command\VideoVote\CreateOrUpdateVideoVoteCommand;
use App\Domain\Entity\User;


/**
 * Class VideoVoteVoter
 *
 * @author <thecold147@gmail.com>
 */
class VideoVoteVoter extends AbstractVoter
{

    public function isGranted($user, $className, $object)
    {
        return $user instanceof User && $user->getId() === (int)$object->authorId;
    }

    public function supportsClass($class)
    {
        return CreateOrUpdateVideoVoteCommand::class === $class;
    }
}
