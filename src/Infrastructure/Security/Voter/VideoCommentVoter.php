<?php

namespace App\Infrastructure\Security\Voter;

use App\Application\Command\VideoComment\CreateVideoCommentCommand;
use App\Application\Command\VideoComment\DeleteVideoCommentByIdCommand;
use App\Application\Command\VideoComment\GetVideoCommentByIdCommand;
use App\Application\Command\VideoComment\UpdateVideoCommentByIdCommand;
use App\Domain\Entity\User;


/**
 * Class VideoCommentVoter
 *
 * @author <thecold147@gmail.com>
 */
class VideoCommentVoter extends AbstractVoter
{

    public function isGranted($user, $className, $object)
    {
        switch ($className) {
            case CreateVideoCommentCommand::class:
            case UpdateVideoCommentByIdCommand::class:
            case DeleteVideoCommentByIdCommand::class:
                return $user instanceof User && $user->getId() === (int)$object->authorId;
            default:
                return true;
        }
    }

    public function supportsClass($class)
    {
        return in_array($class, [
            CreateVideoCommentCommand::class,
            UpdateVideoCommentByIdCommand::class,
            DeleteVideoCommentByIdCommand::class,
            GetVideoCommentByIdCommand::class
        ]);
    }
}
