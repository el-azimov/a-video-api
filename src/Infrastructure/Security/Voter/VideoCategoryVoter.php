<?php

namespace App\Infrastructure\Security\Voter;

use App\Application\Command\VideoCategory\CreateVideoCategoryCommand;
use App\Application\Command\VideoCategory\GetVideoCategoriesCommand;
use App\Application\Command\VideoCategory\GetVideoCategoryByIdCommand;
use App\Application\Command\VideoCategory\GetVideoCategoryByNameCommand;
use App\Application\Command\VideoCategory\UpdateVideoCategoryByIdCommand;
use App\Application\Command\VideoCategory\UpdateVideoCategoryByNameCommand;

/**
 * Class VideoCategoryVoter
 *
 * @author <thecold147@gmail.com>
 */
class VideoCategoryVoter extends AbstractVoter
{
    public function supportsClass($class)
    {
        return in_array($class, [
            CreateVideoCategoryCommand::class,
            GetVideoCategoryByIdCommand::class,
            GetVideoCategoryByNameCommand::class,
            UpdateVideoCategoryByIdCommand::class,
            UpdateVideoCategoryByNameCommand::class,
            GetVideoCategoriesCommand::class,
        ]);
    }

    public function isGranted($user, $className, $object)
    {
        switch ($className) {
            case CreateVideoCategoryCommand::class:
            case UpdateVideoCategoryByIdCommand::class:
            case UpdateVideoCategoryByNameCommand::class:
                return false;
            default:
                return true;
        }
    }
}
