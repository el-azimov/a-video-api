<?php

namespace App\Infrastructure\Security\Voter;

use App\Application\Command\Security\LoginCommand;
use App\Domain\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;


/**
 * Class SecurityVoter
 *
 * @author <thecold147@gmail.com>
 */
class SecurityVoter implements VoterInterface
{
    private $roleVoter;

    public function __construct(VoterInterface $roleVoter)
    {
        $this->roleVoter = $roleVoter;
    }
    
    public function supportsAttribute($attribute)
    {
        return true;
    }

    public function supportsClass($class)
    {
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        if (
            self::ACCESS_GRANTED === $this->roleVoter->vote($token, $object, [User::ROLE_ADMIN]) ||
            get_class($object) === LoginCommand::class
        ) {
            return self::ACCESS_GRANTED;
        } else {
            return self::ACCESS_ABSTAIN;
        }
    }
}
