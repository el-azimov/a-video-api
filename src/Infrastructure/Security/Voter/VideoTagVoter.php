<?php

namespace App\Infrastructure\Security\Voter;

use App\Application\Command\VideoTag\CreateVideoTagCommand;
use App\Application\Command\VideoTag\DeleteVideoTagByIdCommand;
use App\Application\Command\VideoTag\GetVideoTagByIdCommand;
use App\Application\Command\VideoTag\GetVideoTagsCommand;
use App\Application\Command\VideoTag\UpdateVideoTagByIdCommand;


/**
 * Class VideoTagVoter
 *
 * @author <thecold147@gmail.com>
 */
class VideoTagVoter extends AbstractVoter
{
    public function supportsClass($class)
    {
        return in_array($class, [
            CreateVideoTagCommand::class,
            UpdateVideoTagByIdCommand::class,
            GetVideoTagByIdCommand::class,
            DeleteVideoTagByIdCommand::class,
            GetVideoTagsCommand::class,
        ]);
    }

    public function isGranted($user, $className, $object)
    {
        switch ($className) {
            case CreateVideoTagCommand::class:
            case UpdateVideoTagByIdCommand::class:
            case DeleteVideoTagByIdCommand::class:
                return false;
            default:
                return true;
        }
    }
}
