<?php

namespace App\Infrastructure\Validator;

use App\Infrastructure\Validator\Constraint\SemanticDateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


/**
 * Class SemanticDateTimeValidator
 *
 * @property \Symfony\Component\Validator\Context\ExecutionContext $context
 * @author <thecold147@gmail.com>
 */
class SemanticDateTimeValidator extends ConstraintValidator
{
    const PRETTY_DATE = \DateTime::ISO8601;

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed            $value      The value that should be validated
     * @param SemanticDateTime $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value) {
            return;
        }
        if (!$value instanceof \DateTime) {
            try {
                $value = new \DateTime($value);
            } catch (\Exception $e) {
                $this->context
                    ->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $value)
                    ->setParameter('{{ min }}', $constraint->min->format(self::PRETTY_DATE))
                    ->setParameter('{{ max }}', $constraint->max->format(self::PRETTY_DATE))
                    ->addViolation();
                return;
            }
        }
        if ($constraint->min && $value < $constraint->min) {
            $this->context
                ->buildViolation($constraint->minMessage)
                ->setParameter('{{ value }}', $value->format(self::PRETTY_DATE))
                ->setParameter('{{ limit }}', $constraint->min->format(self::PRETTY_DATE))
                ->addViolation();
        }
        if ($constraint->max && $value > $constraint->max) {
            $this->context
                ->buildViolation($constraint->maxMessage)
                ->setParameter('{{ value }}', $value->format(self::PRETTY_DATE))
                ->setParameter('{{ limit }}', $constraint->max->format(self::PRETTY_DATE))
                ->addViolation();
        }
    }
}
