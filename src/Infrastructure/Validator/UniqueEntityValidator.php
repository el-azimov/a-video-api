<?php

namespace App\Infrastructure\Validator;

use App\Infrastructure\Validator\Constraint\UniqueEntity;
use Prophecy\Exception\Doubler\MethodNotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class UniqueEntityValidator
 *
 * @property \Symfony\Component\Validator\Context\ExecutionContext $context
 * @author <thecold147@gmail.com>
 */
class UniqueEntityValidator extends ConstraintValidator
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    private function failIfMethodNotFound($object, $method)
    {
        if (!method_exists($object, $method)) {
            throw new MethodNotFoundException(null, get_class($object), $method);
        }
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed        $value      The value that should be validated
     * @param UniqueEntity $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $repo = $this->container->get($constraint->searchFrom);
        $propertyName = $this->context->getPropertyName();
        $method = sprintf($constraint->searchMethod, ucfirst($propertyName));
        $this->failIfMethodNotFound($repo, $method);

        if (0 < count($constraint->searchCurrentValueBy)) {
            if ($constraint->searchCurrentValueFrom) {
                $searchRepo = $this->container->get($constraint->searchCurrentValueFrom);
            } else {
                $searchRepo = $repo;
            }
            $object = $this->context->getObject();
            foreach ($constraint->searchCurrentValueBy as $prop => $val) {
                if (is_string($prop)) {
                    $searchMethod = $val;
                    $searchValue = $object->{$prop};
                } else {
                    $searchValue = $object->{$val};
                    $searchMethod = $method;
                }
                if (!$searchValue) {
                    continue;
                }
                $this->failIfMethodNotFound($searchRepo, $searchMethod);
                $currentObject = $searchRepo->$searchMethod($searchValue);
                if (
                    $currentObject &&
                    method_exists($currentObject, 'toArray') &&
                    $currentObject->toArray()[$propertyName] === $value
                ) {
                    return;
                }
            }
        }
        if ($repo->$method($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
