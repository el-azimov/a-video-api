<?php

namespace App\Infrastructure\Validator\Constraint;

use Symfony\Component\Validator\Constraint;


/**
 * Class SemanticDateTime
 *
 * @author <thecold147@gmail.com>
 */
class SemanticDateTime extends Constraint
{
    public $message = 'This value should be a valid date.';

    public $min;

    public $minMessage = 'This date should be greater than {{ limit }}.';

    public $max;

    public $maxMessage = 'This date should be less than {{ limit }}.';

    public function __construct($options)
    {
        parent::__construct($options);
        if ($this->min) {
            $this->min = new \DateTime($this->min);
        }
        if ($this->max) {
            $this->max = new \DateTime($this->max);
        }
    }


    public function validatedBy()
    {
        return 'app.string_datetime';
    }
}
