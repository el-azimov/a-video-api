<?php

namespace App\Infrastructure\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Class UniqueEntity
 *
 * @author <thecold147@gmail.com>
 */
class UniqueEntity extends Constraint
{
    public $message = 'This value is already used.';

    /**
     * Service name to find the current value. If null then uses the value of $searchFrom.
     *
     * @var string
     */
    public $searchCurrentValueFrom;

    /**
     * Array of fields to find the current value.
     *
     * @examples
    1. ['id' => 'findById', 'currentUsername' => 'findByUsername']
     * 2. ['currentUsername']
     * @var array
     */
    public $searchCurrentValueBy = [];

    /**
     * Service name to find the exist value.
     *
     * @var string
     */
    public $searchFrom;

    /**
     * Service method name to find the exist value. Default findBy<fieldName>
     *
     * @var string
     */
    public $searchMethod = 'findBy%s';

    public function validatedBy()
    {
        return 'app.unique';
    }
}
