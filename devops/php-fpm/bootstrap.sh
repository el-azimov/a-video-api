#!/usr/bin/env bash

set -e

su -m -s /bin/bash -c 'cd /srv; devops/php-fpm/prepare.sh' - www-data

php-fpm
